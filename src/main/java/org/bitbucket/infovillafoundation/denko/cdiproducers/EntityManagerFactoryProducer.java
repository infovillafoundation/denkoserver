package org.bitbucket.infovillafoundation.denko.cdiproducers;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Sandah AUng on 28/2/15.
 */

public class EntityManagerFactoryProducer {

    @Produces
    @ApplicationScoped
    public EntityManagerFactory create() {
        return Persistence.createEntityManagerFactory("DenkoPU");
    }

    public void destroy(@Disposes EntityManagerFactory factory) {
        factory.close();
    }
}