package org.bitbucket.infovillafoundation.denko.mobile.repositories;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;

import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 21/3/15.
 */

@Repository
public interface MessageRepository extends EntityRepository<Message, Long> {
    List<Message> findByMessageDateGreaterThan(Date messageDate);

    @Query("SELECT MAX(m.messageDate) from Message m")
    Date getLastMessageDate();
}
