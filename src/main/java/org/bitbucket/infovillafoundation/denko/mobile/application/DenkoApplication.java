package org.bitbucket.infovillafoundation.denko.mobile.application;

import org.bitbucket.infovillafoundation.denko.mobile.restservice.implementation.*;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Sandah Aung on 22/2/15.
 */

public class DenkoApplication extends Application {
    private Set<Class<?>> classes = new HashSet<Class<?>>();

    public DenkoApplication() {
        classes.add(DefaultDenkoMessageRestService.class);
        classes.add(DefaultDenkoDenkoStationRestService.class);
        classes.add(DefaultDenkoPriceRestService.class);
        classes.add(DefaultDenkoRestService.class);
        classes.add(DefaultDenkoStationReportRestService.class);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
}
