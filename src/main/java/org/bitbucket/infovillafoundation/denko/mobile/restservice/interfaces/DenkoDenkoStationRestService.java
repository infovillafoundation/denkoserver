package org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

@Path("denkoservice/denkostation")
public interface DenkoDenkoStationRestService {

    @GET
    @Produces("application/json")
    public List<DenkoStation> getAllDenkoStations();

    @Path("{id}")
    @GET
    @Produces("application/json")
    public DenkoStation getDenkoStationById(@PathParam("id") long id);

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<DenkoStation> saveDenkoStation(DenkoStation denkoStation);

    @Path("{id}")
    @DELETE
    @Produces("application/json")
    public List<DenkoStation> deleteDenkoStation(@PathParam("id") long id);

    @Path("{id}")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public List<DenkoStation> updateDenkoStation(@PathParam("id") long id, DenkoStation denkoStation);

    @Path("deletable/{id}")
    @GET
    @Produces("application/json")
    public boolean isDeletable(@PathParam("id") long id);

}
