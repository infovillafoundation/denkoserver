package org.bitbucket.infovillafoundation.denko.mobile.restservice.implementation;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoMessageRestService;
import org.bitbucket.infovillafoundation.denko.mobile.service.MessageService;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

public class DefaultDenkoMessageRestService implements DenkoMessageRestService {

    @Inject
    private MessageService messageService;

    public List<Message> getAllMessages() {
        return messageService.getAllMessages();
    }

    public Message getMessageById(@PathParam("id") long id) {
        return messageService.getMessageById(id);
    }

    public List<Message> saveMessage(Message message) {
        return messageService.saveMessage(message);
    }

    public List<Message> deleteMessage(@PathParam("id") long id) {
        return messageService.deleteMessage(id);
    }

    public List<Message> updateMessage(@PathParam("id") long id, Message message) {
        return messageService.updateMessage(id, message);
    }

    @Override
    public boolean isDeletable(long id) {
        return messageService.isDeletable(id);
    }

}
