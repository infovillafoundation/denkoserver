package org.bitbucket.infovillafoundation.denko.mobile.service;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.MessageRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 28/2/15.
 */

@SessionScoped
@Transactional
public class MessageService implements Serializable {

    @Inject
    private MessageRepository messageRepository;

    public List<Message> getAllMessages() {
        return messageRepository.findAll();
    }

    public Message getMessageById(long id) {
        return messageRepository.findBy(id);
    }

    public List<Message> saveMessage(Message message) {
        if (message.getMessageDate() == null)
            message.setMessageDate(new Date());
        messageRepository.save(message);
        return messageRepository.findAll();
    }

    public List<Message> deleteMessage(Message message) {
        messageRepository.remove(message);
        return messageRepository.findAll();
    }

    public List<Message> deleteMessage(long id) {
        messageRepository.remove(messageRepository.findBy(id));
        return messageRepository.findAll();
    }

    public List<Message> updateMessage(long id, Message message) {
        Message messageToUpdate = messageRepository.findBy(id);
        messageToUpdate.setEnglishMessage(message.getEnglishMessage());
        messageToUpdate.setMyanmarMessage(message.getMyanmarMessage());
        messageToUpdate.setMessageDate(message.getMessageDate());
        messageRepository.save(messageToUpdate);
        return messageRepository.findAll();
    }

    public boolean isDeletable(long id) {
        return true;
    }
}
