package org.bitbucket.infovillafoundation.denko.mobile.models;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;

import java.util.List;

/**
 * Created by Sandah Aung on 31/3/15.
 */

public class DenkoModel {

    private List<DenkoStation> denkoStations;

    private List<Message> messages;

    private List<Price> prices;

    private DenkoLastDataState denkoLastDataState;

    public List<DenkoStation> getDenkoStations() {
        return denkoStations;
    }

    public void setDenkoStations(List<DenkoStation> denkoStations) {
        this.denkoStations = denkoStations;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public DenkoLastDataState getDenkoLastDataState() {
        return denkoLastDataState;
    }

    public void setDenkoLastDataState(DenkoLastDataState denkoLastDataState) {
        this.denkoLastDataState = denkoLastDataState;
    }
}
