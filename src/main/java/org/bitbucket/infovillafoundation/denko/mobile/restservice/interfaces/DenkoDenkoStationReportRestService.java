package org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStationReport;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

@Path("denkoservice/denkostationreport")
public interface DenkoDenkoStationReportRestService {

    @GET
    @Produces("application/json")
    public List<DenkoStationReport> getAllDenkoStationReports();

    @Path("{id}")
    @GET
    @Produces("application/json")
    public DenkoStationReport getDenkoStationReportById(@PathParam("id") long id);

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<DenkoStationReport> saveDenkoStationReport(DenkoStationReport denkoStationReport);

    @Path("{id}")
    @DELETE
    @Produces("application/json")
    public List<DenkoStationReport> deleteDenkoStationReport(@PathParam("id") long id);

    @Path("{id}")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public List<DenkoStationReport> updateDenkoStationReport(@PathParam("id") long id, DenkoStationReport denkoStationReport);

    @Path("deletable/{id}")
    @GET
    @Produces("application/json")
    public boolean isDeletable(@PathParam("id") long id);

}
