package org.bitbucket.infovillafoundation.denko.mobile.restservice.implementation;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStationReport;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoDenkoStationReportRestService;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoDenkoStationRestService;
import org.bitbucket.infovillafoundation.denko.mobile.service.DenkoStationReportService;
import org.bitbucket.infovillafoundation.denko.mobile.service.DenkoStationService;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

public class DefaultDenkoStationReportRestService implements DenkoDenkoStationReportRestService {

    @Inject
    private DenkoStationReportService denkoStationReportService;

    public List<DenkoStationReport> getAllDenkoStationReports() {
        return denkoStationReportService.getAllDenkoStationReports();
    }

    public DenkoStationReport getDenkoStationReportById(@PathParam("id") long id) {
        return denkoStationReportService.getDenkoStationReportById(id);
    }

    public List<DenkoStationReport> saveDenkoStationReport(DenkoStationReport denkoStationReport) {
        return denkoStationReportService.saveDenkoStationReport(denkoStationReport);
    }

    public List<DenkoStationReport> deleteDenkoStationReport(@PathParam("id") long id) {
        return denkoStationReportService.deleteDenkoStationReport(id);
    }

    public List<DenkoStationReport> updateDenkoStationReport(@PathParam("id") long id, DenkoStationReport denkoStationReport) {
        return denkoStationReportService.updateDenkoStationReport(id, denkoStationReport);
    }

    @Override
    public boolean isDeletable(long id) {
        return denkoStationReportService.isDeletable(id);
    }

}
