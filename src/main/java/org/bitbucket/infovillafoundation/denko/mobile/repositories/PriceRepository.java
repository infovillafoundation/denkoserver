package org.bitbucket.infovillafoundation.denko.mobile.repositories;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;

import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 21/3/15.
 */

@Repository
public interface PriceRepository extends EntityRepository<Price, Long> {
    List<Price> findByPostDateGreaterThan(Date postDate);

    @Query("SELECT MAX(p.postDate) from Price p")
    Date getLastPostDate();
}
