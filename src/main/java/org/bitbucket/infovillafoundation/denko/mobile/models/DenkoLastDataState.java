package org.bitbucket.infovillafoundation.denko.mobile.models;

import java.util.Date;

/**
 * Created by Sandah Aung on 6/4/15.
 */

public class DenkoLastDataState {

    private long id;

    private Date lastPriceDate;

    private Date lastMessageDate;

    private int totalNumberOfStations;

    private int lastStationId;

    private int stationIdSum;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getLastPriceDate() {
        return lastPriceDate;
    }

    public void setLastPriceDate(Date lastPriceDate) {
        this.lastPriceDate = lastPriceDate;
    }

    public Date getLastMessageDate() {
        return lastMessageDate;
    }

    public void setLastMessageDate(Date lastMessageDate) {
        this.lastMessageDate = lastMessageDate;
    }

    public int getTotalNumberOfStations() {
        return totalNumberOfStations;
    }

    public void setTotalNumberOfStations(int totalNumberOfStations) {
        this.totalNumberOfStations = totalNumberOfStations;
    }

    public int getLastStationId() {
        return lastStationId;
    }

    public void setLastStationId(int lastStationId) {
        this.lastStationId = lastStationId;
    }

    public int getStationIdSum() {
        return stationIdSum;
    }

    public void setStationIdSum(int stationIdSum) {
        this.stationIdSum = stationIdSum;
    }
}
