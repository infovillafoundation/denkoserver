package org.bitbucket.infovillafoundation.denko.mobile.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sandah Aung on 29/3/15.
 */

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String myanmarMessage;

    private String englishMessage;

    @Temporal(TemporalType.TIMESTAMP)
    private Date messageDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMyanmarMessage() {
        return myanmarMessage;
    }

    public void setMyanmarMessage(String myanmarMessage) {
        this.myanmarMessage = myanmarMessage;
    }

    public String getEnglishMessage() {
        return englishMessage;
    }

    public void setEnglishMessage(String englishMessage) {
        this.englishMessage = englishMessage;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }
}
