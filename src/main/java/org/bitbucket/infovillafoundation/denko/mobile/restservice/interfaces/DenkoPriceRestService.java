package org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

@Path("denkoservice/price")
public interface DenkoPriceRestService {

    @GET
    @Produces("application/json")
    public List<Price> getAllPrices();

    @Path("{id}")
    @GET
    @Produces("application/json")
    public Price getPriceById(@PathParam("id") long id);

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<Price> savePrice(Price price);

    @Path("{id}")
    @DELETE
    @Produces("application/json")
    public List<Price> deletePrice(@PathParam("id") long id);

    @Path("{id}")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public List<Price> updatePrice(@PathParam("id") long id, Price price);

    @Path("deletable/{id}")
    @GET
    @Produces("application/json")
    public boolean isDeletable(@PathParam("id") long id);

}
