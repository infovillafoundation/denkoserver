package org.bitbucket.infovillafoundation.denko.mobile.service;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.DenkoStationRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Sandah Aung on 28/2/15.
 */

@SessionScoped
@Transactional
public class DenkoStationService implements Serializable {

    @Inject
    private DenkoStationRepository denkoStationRepository;

    public List<DenkoStation> getAllDenkoStations() {
        return denkoStationRepository.findAll();
    }

    public DenkoStation getDenkoStationById(long id) {
        return denkoStationRepository.findBy(id);
    }

    public List<DenkoStation> saveDenkoStation(DenkoStation denkoStation) {
        denkoStationRepository.save(denkoStation);
        return denkoStationRepository.findAll();
    }

    public List<DenkoStation> deleteDenkoStation(DenkoStation denkoStation) {
        denkoStationRepository.remove(denkoStation);
        return denkoStationRepository.findAll();
    }

    public List<DenkoStation> deleteDenkoStation(long id) {
        denkoStationRepository.remove(denkoStationRepository.findBy(id));
        return denkoStationRepository.findAll();
    }

    public List<DenkoStation> updateDenkoStation(long id, DenkoStation denkoStation) {
        DenkoStation denkoStationToUpdate = denkoStationRepository.findBy(id);
        denkoStationToUpdate.setStationNameEnglish(denkoStation.getStationNameEnglish());
        denkoStationToUpdate.setStationNameMyanmar(denkoStation.getStationNameMyanmar());
        denkoStationToUpdate.setLatitude(denkoStation.getLatitude());
        denkoStationToUpdate.setLongitude(denkoStation.getLongitude());
        denkoStationToUpdate.setStationAddressEnglish(denkoStation.getStationAddressEnglish());
        denkoStationToUpdate.setStationAddressMyanmar(denkoStation.getStationAddressMyanmar());
        denkoStationRepository.save(denkoStationToUpdate);
        return denkoStationRepository.findAll();
    }

    public boolean isDeletable(long id) {
        return true;
    }
}
