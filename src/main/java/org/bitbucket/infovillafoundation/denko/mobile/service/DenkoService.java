package org.bitbucket.infovillafoundation.denko.mobile.service;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;
import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;
import org.bitbucket.infovillafoundation.denko.mobile.models.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.mobile.models.DenkoModel;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.DenkoStationRepository;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.MessageRepository;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.PriceRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 28/2/15.
 */

@SessionScoped
@Transactional
public class DenkoService implements Serializable {

    @Inject
    private DenkoStationRepository denkoStationRepository;

    @Inject
    private MessageRepository messageRepository;

    @Inject
    private PriceRepository priceRepository;

    public DenkoModel getDenkoModel(DenkoLastDataState denkoLastDataState) {

        DenkoModel denkoModel = new DenkoModel();

        List<Message> messages = messageRepository.findByMessageDateGreaterThan(denkoLastDataState.getLastMessageDate());
        if (messages.isEmpty())
            denkoModel.setMessages(null);
        else
            denkoModel.setMessages(messages);

        List<Price> prices = priceRepository.findByPostDateGreaterThan(denkoLastDataState.getLastPriceDate());
        if (prices.isEmpty())
            denkoModel.setPrices(null);
        else
            denkoModel.setPrices(prices);

        int lastId = (int) denkoStationRepository.getLastId();
        int sumId = (int) denkoStationRepository.getSumId();
        int count = (int) denkoStationRepository.count().doubleValue();
        Date lastMessageDate = messageRepository.getLastMessageDate();
        Date lastPriceDate = priceRepository.getLastPostDate();

        if (denkoLastDataState.getLastStationId() != lastId || denkoLastDataState.getTotalNumberOfStations() != count || denkoLastDataState.getStationIdSum() != sumId)
            denkoModel.setDenkoStations(denkoStationRepository.findAll());

        DenkoLastDataState denkoLastDataStateForReturn = new DenkoLastDataState();
        denkoLastDataStateForReturn.setId(1);
        denkoLastDataStateForReturn.setLastStationId(lastId);
        denkoLastDataStateForReturn.setTotalNumberOfStations(count);
        denkoLastDataStateForReturn.setStationIdSum(sumId);
        denkoLastDataStateForReturn.setLastMessageDate(lastMessageDate);
        denkoLastDataStateForReturn.setLastPriceDate(lastPriceDate);

        denkoModel.setDenkoLastDataState(denkoLastDataStateForReturn);

        return denkoModel;
    }
}
