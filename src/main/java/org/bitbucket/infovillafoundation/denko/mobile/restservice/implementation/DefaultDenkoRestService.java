package org.bitbucket.infovillafoundation.denko.mobile.restservice.implementation;

import org.bitbucket.infovillafoundation.denko.mobile.models.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.mobile.models.DenkoModel;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoRestService;
import org.bitbucket.infovillafoundation.denko.mobile.service.DenkoService;

import javax.inject.Inject;

/**
 * Created by Sandah Aung on 22/2/15.
 */

public class DefaultDenkoRestService implements DenkoRestService {

    @Inject
    private DenkoService denkoService;

    @Override
    public DenkoModel getDenkoModel(DenkoLastDataState denkoLastDataState) {
        return denkoService.getDenkoModel(denkoLastDataState);
    }
}
