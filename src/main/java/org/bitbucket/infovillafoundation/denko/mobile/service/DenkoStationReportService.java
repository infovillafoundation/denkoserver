package org.bitbucket.infovillafoundation.denko.mobile.service;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStationReport;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.DenkoStationReportRepository;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.DenkoStationRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Sandah Aung on 28/2/15.
 */

@SessionScoped
@Transactional
public class DenkoStationReportService implements Serializable {

    @Inject
    private DenkoStationReportRepository denkoStationReportRepository;

    public List<DenkoStationReport> getAllDenkoStationReports() {
        return denkoStationReportRepository.findAll();
    }

    public DenkoStationReport getDenkoStationReportById(long id) {
        return denkoStationReportRepository.findBy(id);
    }

    public List<DenkoStationReport> saveDenkoStationReport(DenkoStationReport denkoStationReport) {
        denkoStationReportRepository.save(denkoStationReport);
        return denkoStationReportRepository.findAll();
    }

    public List<DenkoStationReport> deleteDenkoStationReport(DenkoStationReport denkoStationReport) {
        denkoStationReportRepository.remove(denkoStationReport);
        return denkoStationReportRepository.findAll();
    }

    public List<DenkoStationReport> deleteDenkoStationReport(long id) {
        denkoStationReportRepository.remove(denkoStationReportRepository.findBy(id));
        return denkoStationReportRepository.findAll();
    }

    public List<DenkoStationReport> updateDenkoStationReport(long id, DenkoStationReport denkoStationReport) {
        DenkoStationReport denkoStationReportToUpdate = denkoStationReportRepository.findBy(id);
        denkoStationReportToUpdate.setStationName(denkoStationReport.getStationName());
        denkoStationReportToUpdate.setReportId(denkoStationReport.getReportId());
        denkoStationReportToUpdate.setLatitude(denkoStationReport.getLatitude());
        denkoStationReportToUpdate.setLongitude(denkoStationReport.getLongitude());
        denkoStationReportToUpdate.setReportDate(denkoStationReport.getReportDate());
        denkoStationReportRepository.save(denkoStationReportToUpdate);
        return denkoStationReportRepository.findAll();
    }

    public boolean isDeletable(long id) {
        return true;
    }
}
