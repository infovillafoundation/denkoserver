package org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces;

import org.bitbucket.infovillafoundation.denko.mobile.models.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.mobile.models.DenkoModel;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by Sandah Aung on 22/2/15.
 */

@Path("denkoservice/denko")
public interface DenkoRestService {
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public DenkoModel getDenkoModel(DenkoLastDataState denkoLastDataState);
}
