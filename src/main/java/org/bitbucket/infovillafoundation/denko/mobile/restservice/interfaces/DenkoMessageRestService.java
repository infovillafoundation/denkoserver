package org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Message;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

@Path("denkoservice/message")
public interface DenkoMessageRestService {

    @GET
    @Produces("application/json")
    public List<Message> getAllMessages();

    @Path("{id}")
    @GET
    @Produces("application/json")
    public Message getMessageById(@PathParam("id") long id);

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public List<Message> saveMessage(Message message);

    @Path("{id}")
    @DELETE
    @Produces("application/json")
    public List<Message> deleteMessage(@PathParam("id") long id);

    @Path("{id}")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public List<Message> updateMessage(@PathParam("id") long id, Message message);

    @Path("deletable/{id}")
    @GET
    @Produces("application/json")
    public boolean isDeletable(@PathParam("id") long id);

}
