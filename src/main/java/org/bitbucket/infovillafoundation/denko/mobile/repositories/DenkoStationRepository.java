package org.bitbucket.infovillafoundation.denko.mobile.repositories;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;

/**
 * Created by Sandah Aung on 21/3/15.
 */

@Repository
public interface DenkoStationRepository extends EntityRepository<DenkoStation, Long> {

    @Query("SELECT MAX(d.id) from DenkoStation d")
    long getLastId();

    @Query("SELECT SUM(d.id) from DenkoStation d")
    long getSumId();
}
