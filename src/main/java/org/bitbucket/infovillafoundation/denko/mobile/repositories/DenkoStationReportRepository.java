package org.bitbucket.infovillafoundation.denko.mobile.repositories;

import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Query;
import org.apache.deltaspike.data.api.Repository;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStationReport;

/**
 * Created by Sandah Aung on 21/3/15.
 */

@Repository
public interface DenkoStationReportRepository extends EntityRepository<DenkoStationReport, Long> {


}
