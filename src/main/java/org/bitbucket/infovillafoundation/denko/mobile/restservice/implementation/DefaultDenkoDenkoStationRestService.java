package org.bitbucket.infovillafoundation.denko.mobile.restservice.implementation;

import org.bitbucket.infovillafoundation.denko.mobile.entities.DenkoStation;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoDenkoStationRestService;
import org.bitbucket.infovillafoundation.denko.mobile.service.DenkoStationService;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

public class DefaultDenkoDenkoStationRestService implements DenkoDenkoStationRestService {

    @Inject
    private DenkoStationService denkoStationService;

    public List<DenkoStation> getAllDenkoStations() {
        return denkoStationService.getAllDenkoStations();
    }

    public DenkoStation getDenkoStationById(@PathParam("id") long id) {
        return denkoStationService.getDenkoStationById(id);
    }

    public List<DenkoStation> saveDenkoStation(DenkoStation denkoStation) {
        return denkoStationService.saveDenkoStation(denkoStation);
    }

    public List<DenkoStation> deleteDenkoStation(@PathParam("id") long id) {
        return denkoStationService.deleteDenkoStation(id);
    }

    public List<DenkoStation> updateDenkoStation(@PathParam("id") long id, DenkoStation denkoStation) {
        return denkoStationService.updateDenkoStation(id, denkoStation);
    }

    @Override
    public boolean isDeletable(long id) {
        return denkoStationService.isDeletable(id);
    }

}
