package org.bitbucket.infovillafoundation.denko.mobile.service;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;
import org.bitbucket.infovillafoundation.denko.mobile.repositories.PriceRepository;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 28/2/15.
 */

@SessionScoped
@Transactional
public class PriceService implements Serializable {

    @Inject
    private PriceRepository priceRepository;

    public List<Price> getAllPrices() {
        return priceRepository.findAll();
    }

    public Price getPriceById(long id) {
        return priceRepository.findBy(id);
    }

    public List<Price> savePrice(Price price) {
        if (price.getPostDate() == null)
            price.setPostDate(new Date());
        priceRepository.save(price);
        return priceRepository.findAll();
    }

    public List<Price> deletePrice(Price price) {
        priceRepository.remove(price);
        return priceRepository.findAll();
    }

    public List<Price> deletePrice(long id) {
        priceRepository.remove(priceRepository.findBy(id));
        return priceRepository.findAll();
    }

    public List<Price> updatePrice(long id, Price price) {
        Price priceToUpdate = priceRepository.findBy(id);
        priceToUpdate.setRon95(price.getRon95());
        priceToUpdate.setRon92(price.getRon92());
        priceToUpdate.setDieselNormal(price.getDieselNormal());
        priceToUpdate.setDieselSpecial(price.getDieselSpecial());
        priceToUpdate.setPostDate(price.getPostDate());
        priceRepository.save(priceToUpdate);
        return priceRepository.findAll();
    }

    public boolean isDeletable(long id) {
        return true;
    }
}
