package org.bitbucket.infovillafoundation.denko.mobile.restservice.implementation;

import org.bitbucket.infovillafoundation.denko.mobile.entities.Price;
import org.bitbucket.infovillafoundation.denko.mobile.restservice.interfaces.DenkoPriceRestService;
import org.bitbucket.infovillafoundation.denko.mobile.service.PriceService;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by Sandah Aung on 22/2/15.
 */

public class DefaultDenkoPriceRestService implements DenkoPriceRestService {

    @Inject
    private PriceService priceService;

    public List<Price> getAllPrices() {
        return priceService.getAllPrices();
    }

    public Price getPriceById(@PathParam("id") long id) {
        return priceService.getPriceById(id);
    }

    public List<Price> savePrice(Price price) {
        return priceService.savePrice(price);
    }

    public List<Price> deletePrice(@PathParam("id") long id) {
        return priceService.deletePrice(id);
    }

    public List<Price> updatePrice(@PathParam("id") long id, Price price) {
        return priceService.updatePrice(id, price);
    }

    @Override
    public boolean isDeletable(long id) {
        return priceService.isDeletable(id);
    }

}
